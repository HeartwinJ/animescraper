from sources import _9anime, _4anime

sourceList = [
  '9Anime',
  '4Anime'
]

for index, source in enumerate(sourceList):
  print(str(index) + ' -> ' + source)

sourceIndex = int(input('Select Source: '))

if sourceIndex == 0:
  _9anime.scrape()
elif sourceIndex == 1:
  _4anime.scrape()
else:
  print('Invalid Selection!')