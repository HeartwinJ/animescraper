class Episode:
  def __init__(self, number, link):
    self.number = number
    self.link = link

  def __str__(self):
    return 'Episode(Number: ' + str(self.number) + ', Link: ' + self.link + ')'

  def __repr__(self):
    return 'Episode(Number: ' + str(self.number) + ', Link: ' + self.link + ')'
