class Anime:
  def __init__(self, name, image, url, episodes = None):
    self.name = name
    self.image = image
    self.url = url
    self.episodes = episodes

  def __str__(self):
    return 'Anime(Name: ' + self.name + ', Image: ' + self.image + ', URL: ' + self.url + ', Episodes: ' + str(self.episodes) + ')'
