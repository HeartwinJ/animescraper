# AnimeScraper
A simple Anime Scraper that gives you direct video links

## Available Sources

 - 9Anime
 - 4Anime

## Installation

 1. Download or Clone the repository
 2. Install the requirements from `requirements.txt`
 
 ## Usage
 
 1. Run `main.py`
 2. Select your Source
 3. Search for Anime
 4. You will get the list of episodes along with their links
