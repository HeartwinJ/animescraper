import requests
from bs4 import BeautifulSoup
import json

from models import Anime as _anime, Episode as _episode

def scrape():
  search_term = input('Search Anime: ')

  page_url = 'https://9anime.gg/search/?keyword=' + search_term.replace(' ', '+').lower()
  results_page = requests.get(page_url)

  resultsDoc = BeautifulSoup(results_page.content, 'html.parser')

  results = resultsDoc.find(class_='film-list')
  results = results.find_all('div', class_='item')

  anime_results = list()

  for item in results:
    name = " ".join(item.find(class_='name').text.split())
    url = item.find(class_='name')['href']
    image = item.find(class_='poster')
    image = image.find('img')['src']

    anime_results.append(_anime.Anime(name, image, url))

  for index, anime in enumerate(anime_results):
    print(str(index) + ' -> ' + str(anime))

  select_index = int(input('Select Anime: '))

  selected_anime = anime_results[select_index]

  anime_page = requests.get(selected_anime.url)

  animeDoc = BeautifulSoup(anime_page.content, 'html.parser')

  epsList = animeDoc.find(id='list-eps')
  epsList = epsList.find_all('a')

  episodes = list()

  for epNum, episode in enumerate(epsList, start=1):
    episodes.append(_episode.Episode(epNum, episode['data-src']))

  selected_anime.episodes = episodes

  print(selected_anime)

  for episode in selected_anime.episodes:
    episode.link = episode.link.replace('/?', '/api?')
    response = requests.get(episode.link)
    jsonData = json.loads(response.content)
    jsonData['backup']['link'] = jsonData['backup']['link'].replace('-play.net', '-stream.com').replace('streaming.php', 'download')
    episode.link = jsonData['backup']['link']

    print(episode)

  for episode in selected_anime.episodes:
    response = requests.get(episode.link)
    episode_soup = BeautifulSoup(response.content, 'html.parser')
    download_link = episode_soup.find(class_='dowload').find('a')['href']
    episode.link = download_link

    if download_link.find('googleapis') == -1:
      download_link = episode_soup.find(class_='mirror_link').find_all('a')[-1]['href']
      episode.link = download_link
    
    print(episode)