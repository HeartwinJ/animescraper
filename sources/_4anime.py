import requests
from bs4 import BeautifulSoup

from models import Anime as _anime, Episode as _episode

def scrape():
  search_term = input('Search Anime: ')

  page_url = 'https://4anime.to/?s=' + search_term.strip().replace(' ', '+').lower()
  results_page = requests.get(page_url)

  resultsDoc = BeautifulSoup(results_page.content, 'html.parser')

  results = resultsDoc.find_all(id='headerDIV_95')

  anime_results = list()

  for result in results:
    result = result.find('a')
    url = result['href']
    image = result.find('img')['src']
    name = result.find('div').text

    anime_results.append(_anime.Anime(name, image, url))

  for index, anime in enumerate(anime_results):
    print(str(index) + ' -> ' + str(anime))

  select_index = int(input('Select Anime: '))

  selected_anime = anime_results[select_index]

  anime_page = requests.get(selected_anime.url)

  animeDoc = BeautifulSoup(anime_page.content, 'html.parser')

  epsList = animeDoc.find('ul', class_='episodes').find_all('a')

  episodes = list()

  for episode in epsList:
    epNum = episode.text
    epLink = episode['href']
    episodes.append(_episode.Episode(epNum, epLink))

  selected_anime.episodes = episodes

  print(selected_anime)
  print('-----------------------------------------------------------------------')

  for episode in selected_anime.episodes:
    response = requests.get(episode.link)
    episode_soup = BeautifulSoup(response.content, 'html.parser')
    download_link = episode_soup.find('video', id='example_video_1').find('source')['src']
    episode.link = download_link
    
    # print(download_link)
    
    print(episode)